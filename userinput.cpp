#include "userinput.h"
#include "world.h"

UserInput::UserInput(QObject *parent) : QObject(parent)
{

}

bool UserInput::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
//        qDebug("Ate key press %d", keyEvent->key());
        if (keyEvent->key() == Qt::Key_W)
            userObject->addForward(true);
        if (keyEvent->key() == Qt::Key_S)
            userObject->addBackward(true);
        if (keyEvent->key() == Qt::Key_A)
            userObject->addLeft(true);
        if (keyEvent->key() == Qt::Key_D)
            userObject->addRight(true);
        return true;
    }
    if (event->type() == QKeyEvent::KeyRelease) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
//        qDebug("Ate key release %d", keyEvent->key());
        if (keyEvent->key() == Qt::Key_W)
            userObject->addForward(false);
        if (keyEvent->key() == Qt::Key_S)
            userObject->addBackward(false);
        if (keyEvent->key() == Qt::Key_A)
            userObject->addLeft(false);
        if (keyEvent->key() == Qt::Key_D)
            userObject->addRight(false);
    }
    return false;
}

IControl *UserInput::getUserObject() const
{
    return userObject;
}

void UserInput::setUserObject(IControl *value)
{
    userObject = value;
}

