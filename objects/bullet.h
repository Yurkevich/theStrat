#ifndef BULLET_H
#define BULLET_H

#include "worldobject.h"

class Bullet : public WorldObject
{
public:
    Bullet();

    void moveToPoint();
    void move();
    Vector getTarget() const;
    void setTarget(const Vector &value);

    double speed;

};

#endif // BULLET_H
