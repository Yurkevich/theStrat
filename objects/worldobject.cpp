#include "worldobject.h"
#include "world.h"

WorldObject::WorldObject(QObject *parent) : QObject(parent)
{
    _hasCollider = false;
    itemObject = NULL;
    collider = NULL;
    rotationDegree = 0;
    direction = Vector(1, 0);
    leftSideDirection = Vector(0, -1);
}

bool WorldObject::getIsStatic() const
{
    return isStatic;
}

void WorldObject::setIsStatic(bool value)
{
    isStatic = value;
}

void WorldObject::moveToPoint()
{

}

void WorldObject::rotateToDirection(const Vector &value)
{
    Vector normal(-1,0);
    Vector directionVector(value.x - getX(), value.y - getY(), true);
    rotationDegree = normal.findAngleTo(directionVector);
}

void WorldObject::rotateToDirection()
{
    Vector normal(-1,0);
    rotationDegree = normal.findAngleTo(direction);
    // rotate colider points
    if (hasCollider())
        collider->recountShape();
}

void WorldObject::move()
{

}

Vector WorldObject::getDirection() const
{
    return direction;
}

void WorldObject::setDirection(const Vector &value)
{

    if (value.length != 1.0) {
        if(this->type == BulletType)
            qDebug() << "bullet";
        direction = Vector(value.x - getX(), value.y - getY(), true);
        direction.normalize();
    } else {
        direction = value;
    }
    leftSideDirection = Vector(direction.y, direction.x * -1);
    leftSideDirection.length = leftSideDirection.countLength();
}

double WorldObject::getRotationDegree() const
{
    return rotationDegree;
}

void WorldObject::setRotationDegree(double value)
{
    rotationDegree = value;
}

Vector WorldObject::getLeftSideDirection() const
{
    return leftSideDirection;
}

Vector WorldObject::getRightSideDirection() const
{
    Vector result(leftSideDirection.x,
                  leftSideDirection.y * -1);
    return result;
}

void WorldObject::setLeftSideDirection(const Vector &value)
{
    leftSideDirection = value;
}

Collider *WorldObject::getCollider() const
{
    return collider;
}

void WorldObject::setCollider(Collider *value)
{
    collider = value;
    _hasCollider = true;
}

void WorldObject::removeCollider()
{
    delete collider;
    _hasCollider = false;
    collider = NULL;
}

Vector WorldObject::getMoveDirection() const
{
    return moveDirection;
}

void WorldObject::setMoveDirection(const Vector &value)
{
    moveDirection = value;
}

Vector WorldObject::getPos() const
{
    return pos;
}

void WorldObject::setPos(const Vector &value)
{
    pos = value;
    if (!_hasCollider) return;
    collider->setPos(Vector(0,0));
}

void WorldObject::increasePos(const Vector &value)
{
    pos += value;
    if (!_hasCollider) return;
    collider->setPos(Vector(0,0));
}

QQuickItem *WorldObject::getItemObject() const
{
    return itemObject;
}

void WorldObject::setItemObject(QQuickItem *value)
{
    itemObject = value;
}

uint WorldObject::getType() const
{
    return type;
}

void WorldObject::setType(const uint &value)
{
    type = value;
}

void WorldObject::setX(const double &v)
{
//    qDebug() << pos.x << " " << pos.y;
    if (pos.x == v) return;
    pos.x = v;
    if (itemObject == NULL) return;
}

double WorldObject::getX()
{
    return pos.x;
}

void WorldObject::setY(const double &v)
{
    if (pos.y == v) return;
    pos.y = v;
    if (itemObject  == NULL) return;
}

double WorldObject::getY()
{
    return pos.y;
}

QString WorldObject::getGameId()
{
    return id;
}

void WorldObject::setGameId(const QString &v)
{
    if (id == v) return;
    id = v;
    emit gameIdChanged();
}

bool WorldObject::hasCollider()
{
    return _hasCollider;
}

