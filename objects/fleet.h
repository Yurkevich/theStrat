#ifndef FLEET_H
#define FLEET_H

#include "worldobject.h"
#include "icontrol.h"
#include "bullet.h"

class Fleet : public WorldObject, public IControl
{
public:
    Fleet(bool isStatic = false, QObject *parent = 0);
    Fleet(double x, double y, bool isStatic = false, QObject *parent = 0);

    void moveToPoint();
    void move();
    Vector getTarget() const;
    void setTarget(const Vector &value);

    // IControl interface realization
    void addForward(bool state);
    void addBackward(bool state);
    void addLeft(bool state);
    void addRight(bool state);

    bool getIsSelected() const;
    void setIsSelected(bool value);
    void shoot();

private:

    bool hasForwardChange;
    bool hasBackwardChange;
    bool hasLeftStrafeChange;
    bool hasRightStrafeChange;

    Vector target;
    double speedForvard;
    double speedbackward;
    double speedLeft;
    double speedRight;

    double currentRightStrafePower;
    double currentLeftStrafePower;
    double currentForwardPower;
    double currentBackwardPower;

    bool isSelected;

    void setDefaults();
};

#endif // FLEET_H


