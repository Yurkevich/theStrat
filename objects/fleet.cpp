#include "fleet.h"
#include "world.h"

Fleet::Fleet(bool isStatic, QObject *parent)
    : WorldObject(parent)
{
    this->isStatic = isStatic;
    setX(0);
    setY(0);
    target = getPos();

    setDefaults();
}

Fleet::Fleet(double x, double y, bool isStatic, QObject *parent)
    : WorldObject(parent)
{
    this->isStatic = isStatic;
    setX(x);
    setY(y);
    target = getPos();

    setDefaults();
}

void Fleet::moveToPoint()
{
    if (isStatic) return;
    if (target == getPos()) return;
    Vector tempVec(target.x - getX(), target.y - getY(), true);
    tempVec.normalize();

//    increasePos(tempVec * speedStep * glob.dt);
}

bool Fleet::getIsSelected() const
{
    return isSelected;
}

void Fleet::setIsSelected(bool value)
{
    isSelected = value;
}

void Fleet::shoot()
{
    Bullet *bullet = new Bullet();
    bullet->setX(getX());
    bullet->setY(getY());
    bullet->type = WorldObject::BulletType;
    bullet->setDirection(direction);
    bullet->setMoveDirection(direction);
    bullet->setCollider(new Collider(bullet, bullet));
    bullet->getCollider()
            ->setShape(new RoundShape( 5, bullet->getCollider() ));
    theWorld->createObject(bullet);
}

void Fleet::setDefaults()
{
    hasForwardChange = false;
    hasBackwardChange = false;
    hasLeftStrafeChange = false;
    hasRightStrafeChange = false;

    speedForvard = 1;
    speedbackward = 1;
    speedLeft = 0.5;
    speedRight = 0.5;

    currentBackwardPower = 0;
    currentForwardPower = 0;
    currentLeftStrafePower = 0;
    currentRightStrafePower = 0;
}

Vector Fleet::getTarget() const
{
    return target;
}

void Fleet::setTarget(const Vector &value)
{
    if (target == value) return;
    target = value;
}

void Fleet::addForward(bool state)
{
    hasForwardChange = state;
}

void Fleet::addBackward(bool state)
{
    hasBackwardChange = state;
}

void Fleet::addLeft(bool state)
{
    hasLeftStrafeChange = state;
}

void Fleet::addRight(bool state)
{
    hasRightStrafeChange = state;
}

void Fleet::move()
{
    if (hasForwardChange)
        currentForwardPower += 0.05;
    else
        currentForwardPower = 0;

    if (hasBackwardChange)
        currentBackwardPower += 0.05;
    else
        currentBackwardPower = 0;

    if (hasLeftStrafeChange)
        currentLeftStrafePower += 0.01;
    else
        currentLeftStrafePower = 0;

    if (hasRightStrafeChange)
        currentRightStrafePower += 0.01;
    else
        currentRightStrafePower = 0;

    // accept axelerationsa
    Vector leftDirection(direction.y, direction.x * -1, true);
    if (hasForwardChange) {
        moveDirection += direction * currentForwardPower;
    }
    if (hasBackwardChange) {
        moveDirection += direction * currentBackwardPower * -1;
    }
    if (hasLeftStrafeChange) {
        moveDirection += leftDirection * currentLeftStrafePower;
    }
    if (hasRightStrafeChange) {
        moveDirection += leftDirection * currentRightStrafePower * -1;
    }

    increasePos(moveDirection * glob.dt * 0.001);
}

