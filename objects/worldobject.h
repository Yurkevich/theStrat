#ifndef WORLDOBJECT_H
#define WORLDOBJECT_H

#include <QObject>
#include <QTime>
#include <QQuickItem>

#include "vector.h"
#include "globals.h"
#include "collisions/collider.h"

#include <QDebug>

class World;

class WorldObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double x READ getX WRITE setX NOTIFY xChanged)
    Q_PROPERTY(double y READ getY WRITE setY NOTIFY yChanged)
    Q_PROPERTY(QString gameId READ getGameId WRITE setGameId NOTIFY gameIdChanged)
public:
    explicit WorldObject(QObject *parent = 0);

    enum ObjectType{
        DefaultType = 0,
        ShipType,
        ConstructionType,
        BulletType
    };

    QString id;

    void increasePos(const Vector &value);

    /* ---- virtual functions ---- */
    virtual void moveToPoint();
    virtual void setTarget(const Vector &value) = 0;
    virtual Vector getTarget() const = 0;
    virtual void rotateToDirection(const Vector &value);
    virtual void rotateToDirection();
    virtual void move() = 0;

    uint type;

    Vector moveDirection;

    /* ---- Getters/setters ---- */
    double getRotationDegree() const;
    void setRotationDegree(double value);

    Vector getLeftSideDirection() const;
    Vector getRightSideDirection() const;
    void setLeftSideDirection(const Vector &value);

    Vector getDirection() const;
    void setDirection(const Vector &value);

    Vector getPos() const;
    void setPos(const Vector &value);

    QQuickItem *getItemObject() const;
    void setItemObject(QQuickItem *value);

    bool getIsStatic() const;
    void setIsStatic(bool value);

    uint getType() const;
    void setType(const uint &value);

    void setX(const double &v);
    double getX();

    void setY(const double &v);
    double getY();

    QString getGameId();
    void setGameId(const QString &v);

    bool hasCollider();
    Collider *getCollider() const;
    void setCollider(Collider *value);
    void removeCollider();

    Vector getMoveDirection() const;
    void setMoveDirection(const Vector &value);

signals:
    void xChanged();
    void yChanged();
    void gameIdChanged();

protected:
    bool isStatic;
    Vector pos;
    Vector direction;
    Vector leftSideDirection;
    double rotationDegree;
private:
    QQuickItem *itemObject;
    bool _hasCollider;
    Collider *collider;

};

#endif // WORLDOBJECT_H
