#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>
#include <QDebug>

class Vector
{
public:
    Vector();
    Vector(double x, double y, bool countLength = false);


    double x;
    double y;
    double length;

    double findAngleTo(Vector other);
    double findPureAngle(Vector other);
    double countLength();
    void normalize();
    double scalarMult(Vector *other);
    double scalarPseudoMult(Vector *other);
    Vector projectionTo(Vector *other);
    Vector getNormalized();

    Vector& operator+=(const Vector &r);
    Vector& operator-=(const Vector &r);
    Vector& operator*=(const double &r);
};

Vector operator+(const Vector &l, const Vector &r);
Vector operator-(const Vector &l, const Vector &r);
Vector operator*(const double &l, const Vector &r);
Vector operator*(const Vector &l, const double &r);
bool operator==(const Vector &l, const Vector &r);
QDebug operator <<(const QDebug d, const Vector &r);



#endif // VECTOR_H
