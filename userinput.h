#ifndef USERINPUT_H
#define USERINPUT_H

#include <QObject>
#include <QKeyEvent>
#include "icontrol.h"


class UserInput : public QObject
{
    Q_OBJECT
public:
    explicit UserInput(QObject *parent = 0);
    bool eventFilter(QObject *obj, QEvent *event);
    IControl *getUserObject() const;
    void setUserObject(IControl *value);

signals:

public slots:

private:
    IControl *userObject;
};

#endif // USERINPUT_H
