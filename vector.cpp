#include "vector.h"

Vector::Vector()
{
    x = 0;
    y = 0;
    length = 0;
}

Vector::Vector(double x, double y, bool countLength)
{
    this->x = x;
    this->y = y;
    if (!countLength) return;
    this->length = this->countLength();
}

double Vector::countLength()
{
    double powX = pow(x, 2);
    double powY = pow(y, 2);
    double result = sqrt(powX + powY);
    if (result >= 0.999f && result < 1.0)
        result = 1.0;
    return result;
}

void Vector::normalize()
{
    if (length <= 0.0 || length == 1.0) return;
    x = x/length;
    y = y/length;
    length = countLength();
}

double Vector::scalarMult(Vector *other)
{
    return (x * other->x) + (y * other->y);
}

double Vector::scalarPseudoMult(Vector *other)
{
    this->countLength();
    other->countLength();
    return length * other->length * sin(0);
}

Vector Vector::projectionTo(Vector *other)
{
    Vector proj;
    double scal = scalarMult(other);
    double c = pow(other->x, 2) + pow(other->y, 2);
    proj.x = (scal / c) * other->x;
    proj.y = (scal / c) * other->y;
    return proj;
}

Vector Vector::getNormalized()
{
    if (length <= 0.0 || length == 1.0) return *this;
    length = countLength();
    return Vector(x/length, y/length);
}

double Vector::findAngleTo(Vector other)
{
    double angle = findPureAngle(other);
    if (x * other.y < y * other.x)
        angle = 360 - angle;
    return angle;
}

double Vector::findPureAngle(Vector other)
{
    length = countLength();
    other.length = other.countLength();
    normalize();
    other.normalize();
    double scalar = x * other.x + y * other.y;
    return acos(scalar) * 180/3.14;
}

Vector &Vector::operator+=(const Vector &r)
{
    x += r.x;
    y += r.y;
    countLength();
    return *this;
}

Vector &Vector::operator-=(const Vector &r)
{
    x -= r.x;
    y -= r.y;
    countLength();
    return *this;
}

Vector &Vector::operator*=(const double &r)
{
    x = x * r;
    y = y * r;
    return *this;
}

bool operator==(const Vector &l, const Vector &r)
{
    if (l.x == r.x && l.y == r.y) return true;
    return false;
}

QDebug operator <<(const QDebug d, const Vector &r)
{
    d << "{" << r.x << "," << r.y << "}";
    return d;
}

Vector operator+(const Vector &l, const Vector &r)
{
    double rx, ry;
    rx = l.x + r.x;
    ry = l.y + r.y;
    Vector v(rx, ry);
    return v;
}

Vector operator-(const Vector &l, const Vector &r)
{
    double rx, ry;
    rx = l.x - r.x;
    ry = l.y - r.y;
    Vector v(rx, ry);
    return v;
}

Vector operator*(const double &l, const Vector &r)
{
    return Vector(r.x * l, r.y * l);
}

Vector operator*(const Vector &l, const double &r)
{
    return Vector(l.x * r, l.y * r);
}
