#include <QGuiApplication>
#include "uihandler.h"
#include "world.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    UIHandler *h = new UIHandler();
    theWorld = new World(h);
    Q_UNUSED(theWorld);
    Q_UNUSED(h);

    return app.exec();
}

