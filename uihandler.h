#ifndef UIHANDLER_H
#define UIHANDLER_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlComponent>
#include <QQuickWindow>
#include <QVariant>
#include <QQuickItem>
#include <QQmlProperty>
#include "objects/worldobject.h"

class UIHandler : public QObject
{
    Q_OBJECT
public:
    explicit UIHandler(QObject *parent = 0);

    QQmlContext *rootContext;
    void createUiObject(WorldObject *obj);

    /* ---- getter/setter ---- */
    QObject *getMainWindow() const;
    void setMainWindow(QObject *value);

signals:
    void objectMoved(QString, int x, int y);
    void mousePositionChanged(Vector pos);
    void spaceClicked();

public slots:
    void setFocus(QVariant id);
    void onSpaceClicked(QVariant x, QVariant y);
    void onMousePositionChanged(QVariant x, QVariant y);

private:
    QQmlApplicationEngine engine;

    QString focus;

    QObject *mainWindow;
    QQuickItem *mainRect;
//    QObject *testFleet;

    QHash<QString, QQuickItem*> uiObjects;
};

#endif // UIHANDLER_H
