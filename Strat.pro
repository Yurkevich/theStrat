TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    uihandler.cpp \
    world.cpp \
#    worldobject.cpp \
#    fleet.cpp \
    vector.cpp \
    globals.cpp \
#    bullet.cpp \
    userinput.cpp \
    icontrol.cpp \
    camera.cpp \
#    collisionsprocessor.cpp \
    collisions/collisionsprocessor.cpp \
    objects/bullet.cpp \
    objects/fleet.cpp \
    objects/worldobject.cpp \
    collisions/collider.cpp \
    collisions/shape.cpp \
    collisions/rectshape.cpp \
    collisions/roundshape.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    uihandler.h \
    world.h \
#    worldobject.h \
#    fleet.h \
    vector.h \
    globals.h \
#    bullet.h \
    userinput.h \
    icontrol.h \
    camera.h \
#    collisionsprocessor.h \
    collisions/collisionsprocessor.h \
    objects/bullet.h \
    objects/fleet.h \
    objects/worldobject.h \
    collisions/collider.h \
    collisions/shape.h \
    collisions/rectshape.h \
    collisions/roundshape.h

