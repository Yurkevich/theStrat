#include "world.h"

World *theWorld;

World::World(UIHandler *ui, QObject *parent) : QObject(parent)
{
    this->uiHandler = ui;
    mainTimer = new QTimer(this);
    connect(mainTimer, SIGNAL(timeout()),
            this, SLOT(mainLoop()));
    mainTimer->start(0);

    userInput = new UserInput();
    QCoreApplication::instance()->installEventFilter(userInput);

    // -- for testing --

    Fleet *obj = new Fleet(240.0, 217.0);
    obj->id = "fleet";
    obj->setX(240.0);
    obj->setY(217.0);
    obj->type = WorldObject::ShipType;
    obj->setCollider(new Collider(obj, obj));
    obj->getCollider()
            ->setShape(new RectangleShape(Vector(80, 40),
                                          obj->getCollider()));
    createObject(obj);
    userInput->setUserObject(obj);

    Fleet *object = new Fleet(100.0, 300.0);
    object->id = "test";
    object->setX(100.0);
    object->setY(300.0);
    object->type = WorldObject::ConstructionType;
    object->setCollider(new Collider(object, object));
    object->getCollider()
            ->setShape(new RectangleShape(Vector(80, 40),
                                          object->getCollider()));
    createObject(object);

    userCamera = new Camera(obj, this);


    connect(uiHandler, SIGNAL(spaceClicked()),
            this, SLOT(onSpaceClicked()));
    connect(uiHandler, SIGNAL(mousePositionChanged(Vector)),
            this, SLOT(onMousePositionChanged(Vector)));

}

void World::createObject(WorldObject *object)
{

    objects.append(object);
    // TODO TEST!
    uiHandler->createUiObject(object);
}

void World::mainLoop()
{
    mainTimer->stop();
    double elapsTime = et.restart();
    glob.dt = elapsTime / glob.frameRate;
    processWorld();
    mainTimer->start(0);
    QThread::usleep(1);
}

void World::onObjectMoved(QString id, int x, int y)
{
    qDebug() << __FUNCTION__ << id << " " << x << " " << y;
    objects.first()->setTarget(Vector(x, y));
}

void World::onSpaceClicked()
{
    Fleet *ship = (Fleet*)objects.first();
    ship->shoot();
}

void World::onMousePositionChanged(Vector pos)
{
    Vector realPos = pos + userCamera->getPosition();
    objects.first()->setDirection(realPos);
}

void World::processWorld()
{
    foreach (WorldObject* o, objects) {
        o->rotateToDirection();
        o->move();
//        qDebug() << o->getX() << " " << o->getY();
    }
    foreach (WorldObject* o, objects) {
        if (!o->hasCollider()) continue;
        if (!o->getIsStatic()) {
            foreach (WorldObject* o2, objects) {
                if (o == o2) continue;
                if (o2->hasCollider())
                    CollisionsProcessor::detectCollision(o, o2);
            }
        }
    }

    userCamera->drawWorld(&objects);
}

UIHandler *World::getUiHandler() const
{
    return uiHandler;
}

void World::setUiHandler(UIHandler *value)
{
    uiHandler = value;
}
