#ifndef RECTSHAPE_H
#define RECTSHAPE_H

#include <QObject>
#include "shape.h"

class RectangleShape : public Shape
{
    Q_OBJECT
public:
    explicit RectangleShape(QObject *parent = 0);
    explicit RectangleShape(Vector size, QObject *parent = 0);
    explicit RectangleShape(double width,
                         double height,
                         QObject *parent = 0);

    QList<Vector> pointsReal;
    void recountPoints(const Vector &position,
                       const Vector &direction,
                       const Vector &rightSideDirection);


    /* ---- Getters/Setters ---- */
    QList<Vector> getPointsDefault() const;
    void setPointsDefault(const QList<Vector> &value);

    double width();
    double height();
    Vector getSize() const;
    void setSize(const Vector &value);

private:
    QList<Vector> pointsDefault;
    Vector size;

};

#endif // RECTSHAPE_H
