#include "roundshape.h"

RoundShape::RoundShape(QObject *parent)
    : Shape(parent)
{
    setType(Shape::RoundShape);
    this->pos = Vector(0,0);
}

RoundShape::RoundShape(double radius, QObject *parent)
    : Shape(parent)
{
    this->radius = radius;
    setType(Shape::RoundShape);
    this->pos = Vector(0,0);
}

void RoundShape::recountPoints(const Vector &position,
                               const Vector &direction,
                               const Vector &rightSideDirection)
{
    Q_UNUSED(position); Q_UNUSED(direction); Q_UNUSED(rightSideDirection);
}

double RoundShape::getRadius() const
{
    return radius;
}

void RoundShape::setRadius(double value)
{
    radius = value;
}

Vector RoundShape::getPos() const
{
    return pos;
}

void RoundShape::setPos(const Vector &value)
{
    pos = value;
}
