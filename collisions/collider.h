#ifndef COLLIDER_H
#define COLLIDER_H

#include <QObject>
#include "shape.h"
#include "rectshape.h"
#include "roundshape.h"

class WorldObject;

class Collider : public QObject
{
    Q_OBJECT
public:
    explicit Collider(WorldObject *objParent, QObject *parent = 0);
    explicit Collider(WorldObject *objParent,
                      int shapeType,
                      QObject *parent = 0);

    /* getters/setters */
    int getShapeType() const;
    void setShapeType(int value);

    Vector getPos() const;
    void setPos(const Vector &value);

    WorldObject *getParentObj() const;
    void setParentObj(WorldObject *value);

    QList<Shape *> getShapesMult() const;
    void setShapesMult(const QList<Shape *> &value);
    void setShape(Shape *shape);
    /* -- */

    /* Calls shapes real points recount */
    void recountShape();

private:
    /* shape of collider */
    int shapeType;

    /* used to store multypile shapes */
    QList<Shape *> shapesMult;

    /* Position, relative to owner */
    Vector pos;

    /* Collider owner object */
    WorldObject *parentObj;

};

#endif // COLLIDER_H
