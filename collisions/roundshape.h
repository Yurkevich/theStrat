#ifndef ROUNDSHAPE_H
#define ROUNDSHAPE_H

#include <QObject>
#include "shape.h"

class RoundShape : public Shape
{
    Q_OBJECT
public:
    explicit RoundShape(QObject *parent = 0);
    explicit RoundShape(double radius, QObject *parent = 0);
    void recountPoints(const Vector &position,
                       const Vector &direction,
                       const Vector &rightSideDirection);

    double getRadius() const;
    void setRadius(double value);

    Vector getPos() const;
    void setPos(const Vector &value);

private:
    double radius;
    /* relative position */
    Vector pos;
};

#endif // ROUNDSHAPE_H
