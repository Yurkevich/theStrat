#include "collider.h"
#include "objects/worldobject.h"

Collider::Collider(WorldObject *objParent, QObject *parent)
    : QObject(parent)
{
    this->parentObj = objParent;
    setPos(Vector(0,0));
}

Collider::Collider(WorldObject *objParent,
                   int shapeType,
                   QObject *parent)
    : QObject(parent)
{
    this->shapeType = shapeType;
    this->parentObj = objParent;
    setPos(Vector(0,0));
    shapesMult.append(new RectangleShape(this));
}

int Collider::getShapeType() const
{
    return shapeType;
}

void Collider::setShapeType(int value)
{
    shapeType = value;
}

Vector Collider::getPos() const
{
    return pos;
}

void Collider::setPos(const Vector &value)
{
    pos = value + parentObj->getPos();
}

void Collider::recountShape()
{
    for (int i = 0; i < shapesMult.count(); i++) {
        shapesMult.at(i)
                ->recountPoints(pos,
                               parentObj->getDirection(),
                               parentObj->getRightSideDirection());
    }
}

QList<Shape *> Collider::getShapesMult() const
{
    return shapesMult;
}

void Collider::setShapesMult(const QList<Shape *> &value)
{
    if (value.count() > 1)
        shapeType = Shape::MultyShape;
    else
        shapeType = value.first()->getType();
    shapesMult = value;
}

void Collider::setShape(Shape *shape)
{
    if (!shapesMult.isEmpty()) return;
    shapesMult.append(shape);
    this->shapeType = shape->getType();
}

WorldObject *Collider::getParentObj() const
{
    return parentObj;
}

void Collider::setParentObj(WorldObject *value)
{
    parentObj = value;
}
