#ifndef COLLISIONSPROCESSOR_H
#define COLLISIONSPROCESSOR_H

#include <QObject>
#include "objects/worldobject.h"
#include "objects/fleet.h"

class CollisionsProcessor : public QObject
{
    Q_OBJECT
public:
    explicit CollisionsProcessor(QObject *parent = 0);
    static void detectCollision(WorldObject *l, WorldObject *r);


signals:

public slots:

private:
    static void detectCollisionRectRect(WorldObject *l,
                                        WorldObject *r);
    static void detectCollisionCircleRect(WorldObject *l,
                                          WorldObject *r);

    static QPair<Vector, Vector> getProjectionsToVector(QList<Vector> points,
                                                 Vector *vec);
    static QPair<Vector, Vector> getCircleProjectionToVector(Collider *c, Vector *vec);
    static QPair<int, Vector> hasSeparatingAxis(QList<QPair<Vector, Vector> > l,
                           QList<QPair<Vector, Vector> > r);
    static void setMinCollDir(Vector ch1,
                       Vector ch2,
                       QPair<int, Vector> *colVec);
};

#endif // COLLISIONSPROCESSOR_H
