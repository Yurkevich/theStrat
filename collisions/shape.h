#ifndef SHAPE_H
#define SHAPE_H

#include <QObject>
#include "vector.h"

class Shape : public QObject
{
    Q_OBJECT
public:
    explicit Shape(QObject *parent = 0);

    enum ShapeTypes {
        RectangleShape = 0,
        RoundShape,
        MultyShape
    };

    virtual void recountPoints(const Vector &position,
                       const Vector &direction,
                       const Vector &rightSideDirection) = 0;

    int getType() const;
    void setType(int value);

private:
    int type;
};

#endif // SHAPE_H
