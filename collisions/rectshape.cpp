#include "rectshape.h"
#include "collider.h"

RectangleShape::RectangleShape(QObject *parent)
    : Shape(parent)
{
    setType(Shape::RectangleShape);
    for (int i = 0; i < 4; i++) {
        pointsDefault.append(Vector());
        pointsReal.append(Vector());
    }
}

RectangleShape::RectangleShape(Vector size, QObject *parent)
    : Shape(parent)
{
    setType(Shape::RectangleShape);
    this->size = size;
    pointsDefault.append(Vector(size.x / 2, size.y / 2));
    pointsDefault.append(Vector(size.x / 2, -size.y / 2));
    pointsDefault.append(Vector(-size.x / 2, -size.y / 2));
    pointsDefault.append(Vector(-size.x / 2, size.y / 2));

    for (int i = 0; i < pointsDefault.count(); i++)
        pointsReal.append(pointsDefault.at(i)
                          + qobject_cast<Collider*>(parent)->getPos());
}

RectangleShape::RectangleShape(double width,
                         double height,
                         QObject *parent)
    : Shape(parent)
{
    RectangleShape(Vector(height, width));
}

void RectangleShape::recountPoints(const Vector &position,
                                const Vector &direction,
                                const Vector &rightSideDirection)
{
    for (int i = 0; i < 4; i++) {
        pointsReal[i] = direction * pointsDefault.at(i).x
                + rightSideDirection * pointsDefault.at(i).y;
        pointsReal[i] = pointsReal.at(i) + position;
    }
}

QList<Vector> RectangleShape::getPointsDefault() const
{
    return pointsDefault;
}

void RectangleShape::setPointsDefault(const QList<Vector> &value)
{
    pointsDefault = value;
}

double RectangleShape::width()
{
    return this->size.y;
}

double RectangleShape::height()
{
    return this->size.x;
}

Vector RectangleShape::getSize() const
{
    return size;
}

void RectangleShape::setSize(const Vector &value)
{
    size = value;
}
