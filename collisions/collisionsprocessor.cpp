#include "collisionsprocessor.h"

CollisionsProcessor::CollisionsProcessor(QObject *parent) : QObject(parent){}

void CollisionsProcessor::detectCollision(WorldObject *l, WorldObject *r)
{
    if (l->getCollider()->getShapeType() == Shape::RectangleShape &&
        r->getCollider()->getShapeType() == Shape::RectangleShape)
        detectCollisionRectRect(l, r);
    if (l->getCollider()->getShapeType() == Shape::RoundShape &&
        r->getCollider()->getShapeType() == Shape::RectangleShape)
        detectCollisionCircleRect(l, r);
}

void CollisionsProcessor::detectCollisionRectRect(WorldObject *l, WorldObject *r)
{
    Collider *left  = l->getCollider();
    Collider *right = r->getCollider();
    left->recountShape();
    right->recountShape();
    RectangleShape *lshape = qobject_cast<RectangleShape*>
            (left->getShapesMult().first());
    RectangleShape *rshape = qobject_cast<RectangleShape*>
            (right->getShapesMult().first());

    // projections;
    QList<QPair<Vector, Vector> > lLines;
    QList<QPair<Vector, Vector> > rLines;
    // l dir
    Vector lDir = l->getDirection();
    Vector lLeftDir = l->getLeftSideDirection();
    Vector rDir = r->getDirection();
    Vector rLeftDir = r->getLeftSideDirection();
//    qDebug() << lshape->pointsReal;
    lLines.append( getProjectionsToVector(lshape->pointsReal, &lDir) );
    lLines.append( getProjectionsToVector(lshape->pointsReal, &lLeftDir) );
    lLines.append( getProjectionsToVector(lshape->pointsReal, &rDir) );
    lLines.append( getProjectionsToVector(lshape->pointsReal, &rLeftDir) );
    //
    rLines.append( getProjectionsToVector(rshape->pointsReal, &lDir) );
    rLines.append( getProjectionsToVector(rshape->pointsReal, &lLeftDir) );
    rLines.append( getProjectionsToVector(rshape->pointsReal, &rDir) );
    rLines.append( getProjectionsToVector(rshape->pointsReal, &rLeftDir) );
    // ---------------------

    QPair<int, Vector> colAxis = hasSeparatingAxis(lLines, rLines);
    if (colAxis.first != -1) {

        Vector additional = r->getPos() - l->getPos();
        if (additional.findPureAngle(colAxis.second) < 90)
            colAxis.second *= -1;
        l->setPos( l->getPos() + colAxis.second );
        // -----------
        colAxis.second.normalize();
        Vector tempMoveDir = static_cast<Fleet*>(l)->getMoveDirection();
        Vector newDir = tempMoveDir -
                2.0 * colAxis.second *
                (colAxis.second.scalarMult(&tempMoveDir));
        qDebug() << "dir" << newDir;
        static_cast<Fleet*>(l)->setMoveDirection(newDir);
    }
}

void CollisionsProcessor::detectCollisionCircleRect(WorldObject *c, WorldObject *r)
{
    qDebug() << r->id;
    Collider *circle  = c->getCollider();
    Collider *rect = r->getCollider();
    RectangleShape *rShape = qobject_cast<RectangleShape*>(rect->getShapesMult().first());
    RoundShape *cShape = qobject_cast<RoundShape*>(circle->getShapesMult().first());

    circle->recountShape();
    rect->recountShape();

    Vector minV;
    double min = -1;
    for (int i = 0; i < 4; i++) {
        Vector temp = circle->getPos() - rShape->pointsReal.at(i);
        temp.length = temp.countLength();
        if (temp.length > min) continue;
        min = temp.length;
        minV = rShape->pointsReal.at(i);
    }

    Vector cProj = circle->getPos() - minV;
    cProj.length = cProj.countLength();
    cProj.normalize();

    QList<QPair<Vector, Vector> > lLines;
    QList<QPair<Vector, Vector> > rLines;
    Vector rDir = r->getDirection();
    Vector rLeftDir = r->getLeftSideDirection();
    // ---- get projections
    lLines.append( getCircleProjectionToVector( circle, &cProj) );
    lLines.append( getCircleProjectionToVector( circle, &rDir ) );
    lLines.append( getCircleProjectionToVector( circle, &rLeftDir ) );
    //
    rLines.append( getProjectionsToVector( rShape->pointsReal, &cProj) );
    rLines.append( getProjectionsToVector( rShape->pointsReal, &rDir ) );
    rLines.append( getProjectionsToVector( rShape->pointsReal, &rLeftDir ) );
    // ----

    QPair<int, Vector> colAxis = hasSeparatingAxis(lLines, rLines);
    if (colAxis.first != -1) {
        qDebug() << "Colision" << colAxis;
        Vector additional = r->getPos() - c->getPos();
        if (additional.findPureAngle(colAxis.second) < 90)
            colAxis.second *= -1;
        c->setPos( c->getPos() + colAxis.second );
        // -----------
        colAxis.second.normalize();
        Vector tempMoveDir = c->getMoveDirection();
        Vector newDir = tempMoveDir -
                2.0 * colAxis.second *
                (colAxis.second.scalarMult(&tempMoveDir));
//        qDebug() << "dir" << newDir;
        c->setMoveDirection(newDir);
    }
}

QPair<Vector, Vector> CollisionsProcessor::getProjectionsToVector(QList<Vector> points,
                                                                  Vector *vec)
{
    QList<Vector> projections;
    foreach (Vector point, points) {
        Vector proj = point.projectionTo(vec);
        projections.append(proj);
    }
    // lines
    Vector min = projections.first();
    Vector max = projections.first();
    foreach (Vector p, projections) {
        min.length = min.countLength();
        max.length = max.countLength();
        p.length = p.countLength();
        if (min.length > p.length)
            min = p;
        if (max.length < p.length)
            max = p;
    }
    return QPair<Vector, Vector>(min, max);
}

QPair<Vector, Vector> CollisionsProcessor::getCircleProjectionToVector(Collider *c, Vector *vec)
{
    QPair<Vector, Vector> result;
    Vector proj = c->getPos().projectionTo(vec);
    RoundShape* sh = qobject_cast<RoundShape*>(c->getShapesMult().first());
    result.first  = proj + ( *vec * sh->getRadius() );
    result.second = proj - ( *vec * sh->getRadius() );
    result.first.length = result.first.countLength();
    result.second.length = result.second.countLength();
    return result;
}

QPair<int, Vector> CollisionsProcessor::hasSeparatingAxis(QList<QPair<Vector, Vector> > l,
                                                          QList<QPair<Vector, Vector> > r)
{
    //    bool result = true;
        QPair<int, Vector> colVec;
        colVec.first = -1;
        for (int i = 0; i < l.count(); i++) {
            QPair<Vector, Vector> lLine = l.at(i);
            QPair<Vector, Vector> rLine = r.at(i);
            // l proj
            Vector check1 = rLine.first - lLine.first;
            Vector check2 = rLine.second - lLine.first;
            if (check1.scalarMult(&check2) <= 0) {
                setMinCollDir(check1,
                              check2,
                              &colVec);
                colVec.first = i;
                continue;
            }
            check1 = rLine.first - lLine.second;
            check2 = rLine.second - lLine.second;
            if (check1.scalarMult(&check2) <= 0) {
                setMinCollDir(check1,
                              check2,
                              &colVec);
                colVec.first = i;
                continue;
            }
            // r proj
            check1 = lLine.first - rLine.first;
            check2 = lLine.second - rLine.first;
            if (check1.scalarMult(&check2) <= 0) {
                setMinCollDir(check1,
                              check2,
                              &colVec);
                colVec.first = i;
                continue;
            }
            check1 = lLine.first - rLine.second;
            check2 = lLine.second - rLine.second;
            if (check1.scalarMult(&check2) <= 0) {
                setMinCollDir(check1,
                              check2,
                              &colVec);
                colVec.first = i;
                continue;
            }
            return QPair<int, Vector>(-1,Vector());
        }
        return colVec;
}

void CollisionsProcessor::setMinCollDir(Vector ch1, Vector ch2, QPair<int, Vector> *colVec)
{
    ch1.length = ch1.countLength();
    ch2.length = ch2.countLength();
    Vector min = ch1;
    if (min.length > ch2.length) {
        min = ch2;
    }
    if (colVec->second.length > min.length ||
            colVec->first == -1) {
        colVec->second = min;
    }
}
