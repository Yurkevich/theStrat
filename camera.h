#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>
#include "objects/worldobject.h"

class Camera : public QObject
{
    Q_OBJECT
public:
    explicit Camera(QObject *parent = 0);
    explicit Camera(WorldObject *followObject, QObject *parent = 0);

    Vector getPosition() const;
    void setPosition(const Vector &value);

    WorldObject *getFollowObject() const;
    void setFollowObject(WorldObject *value);

    void drawWorld(QList<WorldObject *> *objects);

signals:

public slots:
    void onWindowSizeChanged(QVariant width, QVariant height);
private:
    WorldObject *followObject;
    Vector position;
    double heigth;
    double width;
};

#endif // CAMERA_H
