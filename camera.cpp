#include "camera.h"
#include "world.h"

Camera::Camera(QObject *parent) : QObject(parent)
{}

Camera::Camera(WorldObject *followObject, QObject *parent)
{
    this->followObject = followObject;
    this->position = followObject->getPos();

    bool isConverted;
    QObject *mainWindow = qobject_cast<World*>(parent)->getUiHandler()
            ->getMainWindow();
    width = mainWindow->property("width").toDouble(&isConverted);
    heigth = mainWindow->property("height").toDouble(&isConverted);
    connect(mainWindow, SIGNAL(sizeChanged(QVariant, QVariant)),
            this, SLOT(onWindowSizeChanged(QVariant,QVariant)));
}

Vector Camera::getPosition() const
{
    return position;
}

void Camera::setPosition(const Vector &value)
{
    position = value;
}

WorldObject *Camera::getFollowObject() const
{
    return followObject;
}

void Camera::setFollowObject(WorldObject *value)
{
    followObject = value;
}

void Camera::drawWorld(QList<WorldObject *> *objects)
{
    position = followObject->getPos();
    position.x -= width / 2;
    position.y -= heigth / 2;
//    position.x = 0;
//    position.y = 0;
//    qDebug() << position;
    foreach (WorldObject* o, *objects) {
        Vector objPos = o->getPos() - position;
        QQuickItem *item = o->getItemObject();
        item->setProperty("x", objPos.x - item->width()/2);
        item->setProperty("y", objPos.y - item->height()/2);
        item->setProperty("rotation", o->getRotationDegree());
//        if (o->id == "fleet")
//            qDebug() << objPos;
    }
}

void Camera::onWindowSizeChanged(QVariant width, QVariant height)
{
    bool isConverted;
    this->width  = width.toDouble(&isConverted);
    this->heigth = height.toDouble(&isConverted);
}

