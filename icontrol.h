#ifndef ICONTROL_H
#define ICONTROL_H

#include <QObject>

class IControl
{
public:
    explicit IControl();
    virtual void addForward(bool) = 0;
    virtual void addBackward(bool) = 0;
    virtual void addLeft(bool) = 0;
    virtual void addRight(bool) = 0;

signals:

public slots:
};

#endif // ICONTROL_H
