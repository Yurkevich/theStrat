#ifndef WORLD_H
#define WORLD_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QThread>
#include <QElapsedTimer>
#include <QPair>

#include "objects/fleet.h"
#include "uihandler.h"
#include "userinput.h"
#include "camera.h"
#include "collisions/collisionsprocessor.h"

class World : public QObject
{
    Q_OBJECT
public:
    explicit World(UIHandler *ui, QObject *parent = 0);

    void createObject(WorldObject *object);

    UIHandler *getUiHandler() const;
    void setUiHandler(UIHandler *value);

signals:

public slots:
    void mainLoop();
    void onObjectMoved(QString id, int x, int y);
    void onSpaceClicked();
    void onMousePositionChanged(Vector pos);

private:
    QTimer *mainTimer;

    double oldTime;
    QElapsedTimer et;

    void processWorld();

    QList<WorldObject*> objects;

    UIHandler *uiHandler;
    UserInput *userInput;

    Camera *userCamera;

};

extern World *theWorld;

#endif // WORLD_H
