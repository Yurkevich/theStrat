import QtQuick 2.5
import QtQuick.Window 2.2
import "qrc:/Objects/"

Window {
    visible: true
    width: 800
    height: 600
    signal sizeChanged(var width, var height)
    onWidthChanged: {
        sizeChanged(width, height)
    }
    onHeightChanged: {
        sizeChanged(width, height)
    }

    Rectangle {
        width: 10
        height: 10
        x: 10
        y: 100
        color: "red"
    }
    Rectangle {
        width: 10
        height: 10
        x: 100
        y: 10
        color: "blue"
    }

    Rectangle {
        z: -1;
        anchors.fill: parent
        objectName: "mainRect"
        color: "black"
        signal spaceClicked(var x, var y);
        signal mousePositionChanged(var x, var y);
        MouseArea {
            hoverEnabled: true
            anchors.fill: parent
            onPressed: {
                parent.spaceClicked(mouseX, mouseY);
            }

            onClicked: {

            }
            onPositionChanged: {
                parent.mousePositionChanged(mouseX, mouseY)
            }
        }
    }
}
