import QtQuick 2.0

Item {
    rotation: 0
    width: 80
    height: 40

    signal setFocus(var id);

    Rectangle {
        anchors.fill: parent
        color: "green"
//        radius: 50
        Rectangle {
            color: "blue"
            width: 10
            height: 10
            x: 5
            radius: 45
            anchors.verticalCenter: parent.verticalCenter
        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("clicked");
            parent.setFocus(parent.gameid);
        }
    }
}
