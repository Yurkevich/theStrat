#include "uihandler.h"

UIHandler::UIHandler(QObject *parent) : QObject(parent)
{
    qmlRegisterType<WorldObject>();
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    rootContext = engine.rootContext();
    mainWindow = qobject_cast<QQuickWindow*>( engine.rootObjects().at(0) );
    mainRect = mainWindow->findChild<QQuickItem*>("mainRect");

    connect(mainRect, SIGNAL(spaceClicked(QVariant, QVariant)),
            this, SLOT(onSpaceClicked(QVariant,QVariant)));
    connect(mainRect, SIGNAL(mousePositionChanged(QVariant, QVariant)),
            this, SLOT(onMousePositionChanged(QVariant,QVariant)));
}

void UIHandler::createUiObject(WorldObject *obj)
{
    QQmlComponent component(&engine);
    if (obj->type == WorldObject::ShipType)
        component.loadUrl(QUrl(QStringLiteral("qrc:/Objects/qml/WorldObject.qml")));
    if (obj->type == WorldObject::BulletType)
        component.loadUrl(QUrl(QStringLiteral("qrc:/Objects/qml/Bullet.qml")));
    if (obj->type == WorldObject::ConstructionType)
        component.loadUrl(QUrl(QStringLiteral("qrc:/Objects/qml/WorldObject.qml")));

    QObject *object = component.create();
    QQmlEngine::setObjectOwnership(object,
                                   QQmlEngine::CppOwnership);
    QQuickItem *item = qobject_cast<QQuickItem*>(object);
    item->setParentItem(mainRect);
    QQmlProperty(object, "gameId").write(obj->id);
    //TODO: FIX THIS SHIT!
//    QQmlProperty(object, "x").write(obj->getX() - item->height()/2);
//    QQmlProperty(object, "y").write(obj->getY() - item->width()/2);

    //-------
    obj->setItemObject(item);
    uiObjects.insert(obj->id, item);
}

void UIHandler::setFocus(QVariant id)
{
    this->focus = id.toString();
}

void UIHandler::onSpaceClicked(QVariant x, QVariant y)
{
//    qDebug() << x << " " <<y;
//    bool isConverted;
//    emit objectMoved(focus, x.toInt(&isConverted), y.toInt(&isConverted));
    emit spaceClicked();
}

void UIHandler::onMousePositionChanged(QVariant x, QVariant y)
{
    Vector test(1, 0);
    bool isConverted;
    int lx = x.toInt(&isConverted);
    int ly = y.toInt(&isConverted);
    emit mousePositionChanged(Vector(lx, ly));
}

QObject *UIHandler::getMainWindow() const
{
    return mainWindow;
}

void UIHandler::setMainWindow(QObject *value)
{
    mainWindow = value;
}

